# Contrat de cession de vente de voiture Belgique

Modèle en français de contrat de cession de voiture entre particulier et professionel en Belgique.

Ceci est en aucun cas officiel, et l'auteur ne peut être tenu responsable de quelconques informations légales manquantes.

Un exemple peut être téléchargé [ici](https://gitlab.com/dannywillems/latex-contrat-de-cession-voiture/-/jobs/artifacts/master/raw/_build/contrat.pdf?job=build)

## Compiler

```
latexmk -pdf -output-directory=_build contrat.tex
```

Ouvrir `_build/contrat.pdf`
